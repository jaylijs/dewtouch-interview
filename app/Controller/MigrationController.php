<?php

class MigrationController extends AppController{
	
	var $uses = array('Member', 'Transaction');
	
	public function q1(){

		$this->setFlash('Question: Migration of data to multiple DB table');


		// $this->set('title',__('Question: Please change Pop Up to mouse over (soft click)'));
	}

	public function q1_instruction(){

		$this->setFlash('Question: Migration of data to multiple DB table');



		// $this->set('title',__('Question: Please change Pop Up to mouse over (soft click)'));
	}

	public function index(){

		
		
		$this->set('title', __('Migration Test'));

		
		// Get Raw Uploaded Data
		$uploadedData = [];
		if ($this->request->is('post')) {
            if(!empty($this->request->data['FileUpload']['file'])){
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				$mime = finfo_file($finfo, $this->request->data['FileUpload']['file']['tmp_name']);
				//$this->request->data['file']['tmp_name']
				if (in_array($mime, ['text/csv', 'text/plain'])) {
					// Parse and store file
					ini_set('auto_detect_line_endings',TRUE);
					if (($handle = fopen($this->request->data['FileUpload']['file']['tmp_name'], "r")) !== FALSE) {
						$row = 0;
						while (( $data = fgetcsv ( $handle , 1000 , ',')) !== FALSE ) 
						{
							if ($row == 0) {
								$titles = $data;
							} else {
								// Skip title row
								$d = [];
								foreach ($data as $key => $value) {
									$d[str_replace(".", "", str_replace(" ", "_", strtolower($titles[$key])))] = $data[$key];
								}
								$uploadedData[] = $d;
							}
							$row++;
						}
						fclose($handle);
					}
				}
				finfo_close($finfo);
            }
        
		
			date_default_timezone_set('UTC');
			$modified = new DateTime();
			$modified = $modified->format('Y-m-d H:i:s');

			/*
			 *
			 * Prepare Members Database
			 *
			 */
			$members = [];
			foreach($uploadedData as $key => $u) {
				$m = [];
				$split = explode(' ', $u['member_no']);
				$m['type'] = $split[0] ? $split[0] : null;
				$m['no'] = intval($split[1]) ? intval($split[1]) : null;
				$m['name'] = $u['member_name'] ? $u['member_name'] : null;
				$m['company'] = $u['member_company'] ? $u['member_company'] : null;
				$m['valid'] = 1;
				$m['created'] = $modified;
				$m['modified'] = $modified;

				$members[] = $m;
			}

			// Check for overlab -> identify by Member No.
			$unique = array_unique(array_column($members, 'no'));
			$members = array_values(array_intersect_key($members, $unique));

			// Get members from database
			$membersDB = $this->Member->find('list', array(
				'recursive' => -1,
				'fields' => array('Member.no')
			));

			// Remove existing members
			foreach ($members as $key => $val) {
				if (in_array($val['no'], $membersDB)) {
					unset($members[$key]);
				}
			}

			$this->Member->saveAll(array_values($members));


			/*
			 *
			 * Prepare Transactions & TransactionItem Database
			 *
			 */

			// Get members from database
			$membersDB = $this->Member->find('list', array(
				'recursive' => -1,
				'fields' => array('Member.id', 'Member.no')
			));

			// Key by Member.no
			$member_keys = [];
			foreach ($membersDB as $key => $val) {
				$member_keys[intval($val)] = $key;
			}

			// Attach member id to $uploadedData
			foreach($uploadedData as $key => $u) {
				$member_no = intval(explode(' ', $u['member_no'])[1]);
				$uploadedData[$key]['member_id'] = $member_keys[$member_no];
			}

			// Get Transactions Data
			$transactions = [];
			$transactionItems = [];
			foreach($uploadedData as $key => $u) {
				// Skip first record - Already created
				if ($key == 0) { continue; }

				$date = date_create_from_format('d-m-y', $u['date']);
				// Prepare Transaction
				$t = array(
					'Transaction' => array(
						'member_id' => $u['member_id'],
						'member_name' => $u['member_name'] ? $u['member_name'] : null,
						'member_paytype' => $u['member_pay_type'] ? $u['member_pay_type'] : null,
						'member_company' => $u['member_company'] ? $u['member_company'] : null,
						'date' => $date->format('Y-m-d'),
						'year' => $date->format('Y'),
						'month' => $date->format('n'),
						'ref_no' => $u['ref_no'] ? $u['ref_no'] : null,
						'receipt_no' => $u['receipt_no'] ? $u['receipt_no'] : null,
						'payment_method' => $u['payment_by'] ? $u['payment_by'] : null,
						'batch_no' => $u['batch_no'] ? $u['batch_no'] : null,
						'cheque_no' => $u['cheque_no'] ? $u['cheque_no'] : null,
						'payment_type' => $u['payment_description'] ? $u['payment_description'] : null,
						'renewal_year' => $u['renewal_year'] ? $u['renewal_year'] : null,
						'remarks' => null,
						'subtotal' => $u['subtotal'] ? $u['subtotal'] : null,
						'tax' => $u['totaltax'] ? $u['totaltax'] : null,
						'total' => $u['total'] ? $u['total'] : null,
						'valid' => 1,
						'created' => $modified,
						'modified' => $modified
					),
					'TransactionItem' => array(
						0 => array(
							'description' => 'Being Payment for : ' . $u['payment_description'] . ' : ' . $u['renewal_year'],
							'quantity' => 1,
							'unit_price' => $u['subtotal'] ? $u['subtotal'] : null,
							'uom' => null,
							'sum' => $u['subtotal'] ? $u['subtotal'] : null,
							'valid' => 1,
							'created' => $modified,
							'modified' => $modified,
							'table' => 'Member',
							'table_id' => 1
						)
					)
				);

				$transactions[] = $t;

			}

			$this->Transaction->saveMany($transactions, array('deep' => true));
		}
	}

}