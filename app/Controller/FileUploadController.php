<?php

class FileUploadController extends AppController {
	public function index() {
		$this->set('title', __('File Upload Answer'));

        if ($this->request->is('post')) {
            if(!empty($this->request->data['FileUpload']['file'])){
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				$mime = finfo_file($finfo, $this->request->data['FileUpload']['file']['tmp_name']);
				
				//$this->request->data['file']['tmp_name']
				if (in_array($mime, ['text/csv', 'text/plain'])) {
					// Parse and store file
					ini_set('auto_detect_line_endings',TRUE);
					if (($handle = fopen($this->request->data['FileUpload']['file']['tmp_name'], "r")) !== FALSE) {
						$row = 0;
						/*$array = [];*/
						while (( $data = fgetcsv ( $handle , 1000 , ',')) !== FALSE ) 
						{
							if ($row != 0) {
								// Skip title row
								$d = [
									'name' => $data[0],
									'email' => $data[1],
									'created' => date("Y-m-d H:i:s")
								];
								$this->FileUpload->create();
								$this->FileUpload->save($d);
							}
							$row++;
						}
						fclose($handle);
					}
				}
				finfo_close($finfo);
            }
        }
		
		$file_uploads = $this->FileUpload->find('all');
		$this->set(compact('file_uploads'));
	}
}