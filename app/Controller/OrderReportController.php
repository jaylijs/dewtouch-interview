<?php
	class OrderReportController extends AppController{

		public function index(){

			$this->setFlash('Multidimensional Array.');

			$this->loadModel('Order');
			$orders = $this->Order->find('all',array('conditions'=>array('Order.valid'=>1),'recursive'=>2));
			// debug($orders);exit;

			$this->loadModel('Portion');
			$portions = $this->Portion->find('all',array('conditions'=>array('Portion.valid'=>1),'recursive'=>2));
			//debug($portions);exit;

			$this->loadModel('Item');
			$items = $this->Item->find('all',array('conditions'=>array('Item.valid'=>1),'recursive'=>2));
			//debug($items);exit;
			
			// Add portions to items array
			foreach($portions as $key => $p) {
				//debug($p);exit;
				$items[$p['Portion']['item_id'] - 1]['PortionDetail'] = $p['PortionDetail'];
			}
			
			// loop through the create Report
			$report = [];
			foreach ($orders as $key => $o) {
				$report[$o['Order']['name']] = [];
				
				foreach ($o['OrderDetail'] as $key1 => $od) {
					$qty = $od['quantity'];
					$item = $items[$od['item_id'] - 1];
					foreach ($item['PortionDetail'] as $key2 => $pd) {
						$value = $pd['value'];
						
						if (isset($report[$o['Order']['name']][$pd['Part']['name']])) {
							$report[$o['Order']['name']][$pd['Part']['name']] += ($value * $qty);
						} else {
							$report[$o['Order']['name']][$pd['Part']['name']] = $value * $qty;
						}
					}
				}
			}
			
			// Sort Arrays Alphabetically
			foreach ($report as $key => $r) {
				ksort($r);
				$report[$key] = $r;
			}
			
			$order_reports = $report;
			
			// To Do - write your own array in this format
			/*$order_reports = array('Order 1' => array(
										'Ingredient A' => 1,
										'Ingredient B' => 12,
										'Ingredient C' => 3,
										'Ingredient G' => 5,
										'Ingredient H' => 24,
										'Ingredient J' => 22,
										'Ingredient F' => 9,
									),
								  'Order 2' => array(
								  		'Ingredient A' => 13,
								  		'Ingredient B' => 2,
								  		'Ingredient G' => 14,
								  		'Ingredient I' => 2,
								  		'Ingredient D' => 6,
								  	),
								);*/

			// ...

			$this->set('order_reports',$order_reports);

			$this->set('title',__('Orders Report'));
		}

		public function Question(){

			$this->setFlash('Multidimensional Array.');

			$this->loadModel('Order');
			$orders = $this->Order->find('all',array('conditions'=>array('Order.valid'=>1),'recursive'=>2));

			// debug($orders);exit;

			$this->set('orders',$orders);

			$this->loadModel('Portion');
			$portions = $this->Portion->find('all',array('conditions'=>array('Portion.valid'=>1),'recursive'=>2));
				
			// debug($portions);exit;

			$this->set('portions',$portions);

			$this->set('title',__('Question - Orders Report'));
		}

	}