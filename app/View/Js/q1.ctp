<div class="alert  ">
<button class="close" data-dismiss="alert"></button>
Question: Advanced Input Field</div>

<p>
1. Make the Description, Quantity, Unit price field as text at first. When user clicks the text, it changes to input field for use to edit. Refer to the following video.

</p>


<p>
2. When user clicks the add button at left top of table, it wil auto insert a new row into the table with empty value. Pay attention to the input field name. For example the quantity field

<?php echo htmlentities('<input name="data[1][quantity]" class="">')?> ,  you have to change the data[1][quantity] to other name such as data[2][quantity] or data["any other not used number"][quantity]

</p>



<div class="alert alert-success">
<button class="close" data-dismiss="alert"></button>
The table you start with</div>

<table id ="tableID" class="table table-striped table-bordered table-hover">
<thead>
<th><span id="add_item_button" class="btn mini green addbutton" onclick="addToObj=false">
											<i class="icon-plus"></i></span></th>
<th>Description</th>
<th>Quantity</th>
<th>Unit Price</th>
</thead>

<tbody>
	<tr>
	<td></td>
	<td id="1_description" onclick="focusItem('1_description')">
		<span id="1_description_span"></span>
		<textarea id="1_description_input" name="data[1][description]" class="m-wrap hidden description required" rows="2" onblur="hideItem('1_description')"></textarea>
	</td>
	<td id="1_quantity" onclick="focusItem('1_quantity')">
		<span id="1_quantity_span"></span>
		<input id="1_quantity_input" name="data[1][quantity]" class="hidden" onblur="hideItem('1_quantity')">
	</td>
	<td id="1_unit_price" onclick="focusItem('1_unit_price')">
		<span id="1_unit_price_span"></span>
		<input id="1_unit_price_input" name="data[1][unit_price]" class="hidden" onblur="hideItem('1_unit_price')">
	</td>
	
</tr>

</tbody>

</table>


<p></p>
<div class="alert alert-info ">
<button class="close" data-dismiss="alert"></button>
Video Instruction</div>

<p style="text-align:left;">
<video width="78%"   controls>
  <source src="/video/q3_2.mov">
Your browser does not support the video tag.
</video>
</p>


<style>
.table td{
	height: 30px;
	width: 25%;
}
</style>


<?php $this->start('script_own');?>
<script>
var count = 1;
var data = [];
data[1] = [];
data[1]["description"] = "Test";
data[1]["quantity"] = "1";
data[1]["unit_price"] = "1";
	
var focusItem = function (item) {
	$('#' + item + '_span').addClass('hidden');
	$('#' + item + '_input').removeClass('hidden');
	$('#' + item + '_input').focus();
}

var hideItem = function (item) {
	$('#' + item + '_span').removeClass('hidden');
	$('#' + item + '_input').addClass('hidden');
	$('#' + item + '_span').html($('#' + item + '_input').val());
}

$(document).ready(function(){
	
	
	$("#add_item_button").click(function(){
		
		// Create Data Record;
		count += 1;
		data[count] = [];
		data[count]["description"] = null;
		data[count]["quantity"] = null;
		data[count]["unit_price"] = null;
		
		var content = '<tr><td></td><td id="' + count +'_description" onclick="focusItem(\'' + count +'_description\')"><span id="' + count +'_description_span"></span><textarea id="' + count +'_description_input" name="data[' + count + '][description]" class="m-wrap hidden description required" rows="2" onblur="hideItem(\'' + count +'_description\')"></textarea></td><td id="' + count +'_quantity" onclick="focusItem(\'' + count +'_quantity\')"><span id="' + count +'_quantity_span"></span><input id="' + count +'_quantity_input" name="data[' + count + '][quantity]" class="hidden" onblur="hideItem(\'' + count +'_quantity\')"></td><td id="' + count +'_unit_price" onclick="focusItem(\'' + count +'_unit_price\')"><span id="' + count +'_unit_price_span"></span><input id="' + count +'_unit_price_input" name="data[' + count + '][unit_price]" class="hidden" onblur="hideItem(\'' + count +'_unit_price\')"></td></tr>'
		
		// Create new row
		$("#tableID").find('tbody').append(content);
	});
});
</script>
<?php $this->end();?>

