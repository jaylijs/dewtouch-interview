
<div class="row-fluid">
	<table class="table table-bordered" id="table_records">
	</table>
</div>
<?php $this->start('script_own')?>
<script>
$(document).ready(function(){
	
	var data = <?php echo json_encode($records) ?>;
	console.log(data);
	$("#table_records").dataTable({
		"aaData": data,
        "aoColumns": [
            { "sTitle": "ID" },
            { "sTitle": "NAME" }
        ]
	});
})
</script>
<?php $this->end()?>