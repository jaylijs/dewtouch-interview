
<div id="message1">


<?php echo $this->Form->create('Type',array('id'=>'form_type','type'=>'file','class'=>'','method'=>'POST','autocomplete'=>'off','inputDefaults'=>array(
				
				'label'=>false,'div'=>false,'type'=>'text','required'=>false)))?>
	
<?php echo __("Hi, please choose a type below:")?>
<br><br>

<?php $options_new = array(
 		'Type1' => __('<span id="type1" class="d-inline-block showDialog" tabindex="0" data-toggle="tooltip" data-placement="right" data-html="true" title="<li>Description .......</li><li>Description 2</li>"><a href="javascript:;">Type1</a></span>'),
		'Type2' => __('<span id="type2" class="d-inline-block showDialog" tabindex="0" data-toggle="tooltip" data-placement="right" data-html="true" title="<li>Desc 1 .....</li><li>Desc 2...</li>"><a href="javascript:;">Type2</a></span>')
		);?>

<?php echo $this->Form->input('type', array('legend'=>false, 'type' => 'radio', 'options'=>$options_new,'before'=>'<label class="radio line notcheck">','after'=>'</label>' ,'separator'=>'</label><label class="radio line notcheck">'));?>

<?php echo $this->Form->end();?>

<!---->
	
	
</div>

<style>
.showDialog:hover{
	text-decoration: underline;
}

#message1 .radio{
	vertical-align: top;
	font-size: 13px;
}

</style>

<?php $this->start('script_own')?>
<script>

$(document).ready(function(){
	$('#type1').tooltip();
	$('#type2').tooltip();
});


</script>
<?php $this->end()?>