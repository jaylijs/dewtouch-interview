<?php

class Transaction extends AppModel {
	public $hasMany = array('TransactionItem');
	
	var $belongsTo = array('Member');
}